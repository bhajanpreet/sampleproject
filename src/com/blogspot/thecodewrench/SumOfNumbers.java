package com.blogspot.thecodewrench;

import java.util.Scanner;

public class SumOfNumbers {

	public static void main(String[] args) {
		int total = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number");
		int number = sc.nextInt();
		
		for (int i = 0; i <= number; i++) {
			total += i;
		}
		
		System.out.println(total);
		sc.close();
	}
}
