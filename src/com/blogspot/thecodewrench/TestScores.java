package com.blogspot.thecodewrench;

import java.util.Scanner;

public class TestScores {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter the score 1");
		int score1 = keyboard.nextInt();
		System.out.println("Enter the score 2");
		int score2 = keyboard.nextInt();
		System.out.println("Enter the score 3");
		int score3 = keyboard.nextInt();
		int average = (score1 + score2 + score3) / 3;
		System.out.println("The average score is: "+ average);
		keyboard.close();
	}
}
