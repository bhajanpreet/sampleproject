package com.blogspot.thecodewrench;

import java.util.Scanner;

public class SoftwareSales {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter the number softwares you want?");
		int numSoftware = keyboard.nextInt();
		double discount = 0;
		if(numSoftware >= 10 && numSoftware <= 19) {
			discount = 0.2;
		} else if(numSoftware >= 20 && numSoftware <= 49) {
			discount = 0.3;
		} else if(numSoftware >= 50 && numSoftware <= 99) {
			discount = 0.4;
		} else if(numSoftware >= 100){
			discount = 0.5;
		}
		double totalAmount = 99 * numSoftware;
		double discountedAmount = totalAmount * discount;
		double finalAmount = totalAmount - discount;
		System.out.println("SubTotal $"+ totalAmount);
		System.out.printf("Discounted Amount $%.2f\n", discountedAmount);
		System.out.println("Final Amount $"+ finalAmount);
		keyboard.close();
	}

}
