package com.blogspot.thecodewrench;

import java.util.Scanner;

public class MakeSomeDollars {

	public static void main(String[] args) {
		System.out.println("Enter the number of days you are working?");
		Scanner sc = new Scanner(System.in);
		double days = sc.nextDouble();
		double total = 0;
		for (int i = 1; i <= days; i++) {
			System.out.println(Math.pow(2, i - 1));
			total += Math.pow(2, i - 1); 
		}
		System.out.printf("Total: $%.2f", total);
		sc.close();
	}

}
