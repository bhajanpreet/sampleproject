package com.blogspot.thecodewrench;

import java.util.Scanner;

public class Runner {
	public static void main(String[] args) {
	    System.out.println("Enter the number of square feets to convert --> ");
	    Scanner keyboard = new Scanner(System.in);
	    int sq = keyboard.nextInt();
	    System.out.println(sq / 43560.0);
	    keyboard.close();
	}
}
