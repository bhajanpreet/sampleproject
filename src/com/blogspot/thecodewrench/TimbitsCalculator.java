package com.blogspot.thecodewrench;

import java.util.Scanner;

public class TimbitsCalculator {
	public static void main(String[] args) {
		/**
		 * To make 48 Timbits, you need 1.5 cups of
		 * sugar 1 cup of butter 2.75 cups of flour
		 * 
		 * for 48 --> 1.5 sugar 1 of butter and 2.75 of flour 
		 * 
		 * for 1 you need 1.5/48 of s , 1/48 of butter and 2.75/48 of flour
		 * 
		 */
		System.out.println("Enter the amount of Timbits you want to make ?");
		Scanner keyboard = new Scanner(System.in);
		int timbits = keyboard.nextInt();
		System.out.println( (1.5/48) * timbits +" of sugar and "+ (1/48.0) * timbits +""
				+ " of butter and "+ (2.75/48.0) * timbits+" of flour..");
		keyboard.close();
	}
}
